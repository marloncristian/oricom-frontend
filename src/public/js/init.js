document.addEventListener('DOMContentLoaded', function() {
    
    // M.Sidenav.init(document.querySelectorAll('.sidenav'), {});
    // M.Dropdown.init(document.querySelectorAll('#user-option'), {coverTrigger: false, constrainWidth : false});
    // M.Dropdown.init(document.querySelectorAll('#signin-option'), {coverTrigger: false});
    // M.Collapsible.init(document.querySelectorAll('.collapsible'), {});
    // M.Modal.init(document.querySelectorAll('.modal'), {});
    M.AutoInit();
    
    M.Dropdown.init(document.querySelectorAll('#user-option'), {coverTrigger: false, constrainWidth : false});
    M.Dropdown.init(document.querySelectorAll('#signin-option'), {coverTrigger: false});
});
