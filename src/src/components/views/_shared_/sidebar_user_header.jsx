import React from 'react';
import auth from '../../../core/auth';

export default class SidebarUserHeader extends React.Component {
    render = () => {
        var user = auth.GetUser();
        return (
            <div className='mdc-drawer__header mdc-drawer__header-authorized'  style={{ backgroundImage : "url(/img/wallpaper.png)", backgroundPosition : 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover' }}>
                <h3 className='mdc-drawer__title'>{user.name}</h3>
                <h6 className='mdc-drawer__subtitle'>{user.email || "Sem email"}</h6>
                <a href="/private" id="action-login" className="mdc-fab image-avatar" style={{ backgroundImage : "url(" + user.picture_url + ")", backgroundPosition : 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover' }}>
                </a>
            </div>
        )
    }
}