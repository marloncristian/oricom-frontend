import React, { Component } from 'react';
import constants from '../../../../constants';
import auth from '../../../../core/auth';
import { MDCDialog } from '@material/dialog';
import { GoogleLogin } from 'react-google-login';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionAuthStatusChange } from '../../../../state/actions';

class Header extends Component {

    constructor (props) {
        super(props);
        this.state = {
            menuOpen : false,
            dialogOpen : false
        }
    }

    initDialog = (e) => {
        if (!e) return;
        this.dialog = new MDCDialog(e);
    };
    
    onLoginFailure = (error) => {
        this.props.actionAuthStatusChange(false);
    };

    onLoginSuccess = (response) => {
        const payload = {
            access_token: response.tokenObj.access_token, 
            token_type : response.tokenObj.token_type
        };
        const tokenBlob = new Blob([JSON.stringify(payload, null, 2)], {type : 'application/json'});
        const options = {
            method: 'POST',
            body: tokenBlob,
            mode: 'cors',
            cache: 'default'
        };
        fetch('https://oricom-security.herokuapp.com/auth/google/authenticate', options).then(response => {
            response.json().then(body => {
                auth.SignIn(body.access_token, body.user);
                this.props.actionAuthStatusChange(true);
            });
        })
    };

    handleUserClick = (e) => {
        this.setState({menuOpen : !this.state.menuOpen});
    }

    handleEnterClick = (e) => {
        this.dialog.open();
    }

    getUserAction = () => {
        const user = auth.GetUser();
        return (
            <a href="#!" class="material-icons mdc-top-app-bar__action-item" onClick={(e) => this.handleUserClick(e)}>
                <img alt={user.name} width="24" height="24" className="avatar" src={user.picture_url}></img>
            </a>
        )
    }

    getEnterAction = () => {
        return (
            <React.Fragment>
                <button onClick={(e) => this.handleEnterClick(e)} className="mdc-top-app-bar__button mdc-button mdc-button--dense mdc-button--raised mdc-button--primary">
                    <span className="mdc-button__label">entrar</span>
                </button>
                <div id='enter-dialog' className='mdc-dialog' role='alertdialog' aria-modal='true' ref={this.initDialog}>
                    <div className='mdc-dialog__scrim'></div>
                    <div className='mdc-dialog__container'>
                        <div className='mdc-dialog__surface'>
                            <h2 className='mdc-dialog__title-extended mdc-typography--headline6'>Use sua rede social</h2>
                            <section className='mdc-dialog__content'>
                                <ul ref={this.initList} className='mdc-list mdc-list--avatar-list' style={{ listStyleType: 'none' }}>
                                    <GoogleLogin
                                        clientId={constants.ClientID}
                                        onSuccess={this.onLoginSuccess}
                                        onFailure={this.onLoginFailure}
                                        render={renderProps => (
                                            <li className='mdc-list-item' onClick={renderProps.onClick} data-mdc-dialog-action="close">
                                                <i className='oricom_icon-google mdc-list-item__graphic'></i>
                                                <span className='test-list-item__label'>Google</span>
                                            </li>
                                        )}
                                    />
                                    <li className='mdc-list-item'>
                                        <i className='material-icons mdc-list-item__graphic'>person</i>
                                        <span className='test-list-item__label'>Facebook</span>
                                    </li>
                                    <li className='mdc-list-item'>
                                        <i className='material-icons mdc-list-item__graphic'>add</i>
                                        <span className='test-list-item__label'>Twitter</span>
                                    </li>
                                </ul>
                            </section>
                            <footer class="mdc-dialog__actions">
                                <button type="button" class="mdc-button mdc-dialog__button" data-mdc-dialog-action="close">
                                    <span class="mdc-button__label">Cancelar</span>
                                </button>
                            </footer>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    render () {
        const { authenticationStatus } = this.props;
        return (
            <React.Fragment>
                <header id="app-bar" className="mdc-top-app-bar">
                    <div className="mdc-top-app-bar__row">
                        <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
                            <span className="mdc-top-app-bar__title">Oricom</span>
                        </section>
                        <section class="mdc-top-app-bar__section mdc-top-app-bar__section--align-end" role="toolbar">
                            <a href="#!" class="material-icons mdc-top-app-bar__action-item" aria-label="Download" alt="Download">search</a>
                            {authenticationStatus && this.getUserAction()}
                            {!authenticationStatus && this.getEnterAction()}
                        </section>
                    </div>
                </header>
                <div className={this.state.menuOpen ? "oricom_expanded-menu-user__visible" : "oricom_expanded-menu-user__hidden"}>
                    <div>
                        <a href="#!" tabindex="-1"><i class="material-icons" aria-hidden="true">settings</i>Preferências</a>
                        <a href="/private/" tabindex="-1"><i class="material-icons" aria-hidden="true">person</i>Perfil</a>
                        <a href="#!" tabindex="-1"><i class="material-icons" aria-hidden="true">clear</i>Sair</a>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = store => ({ 
    authenticationStatus: store.authenticationState.authenticationStatus 
});
const mapDispatchToProps = dispatch => bindActionCreators ({ actionAuthStatusChange }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(Header);