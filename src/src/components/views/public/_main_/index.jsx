import React, { Component } from 'react';
import Header from './header'

export default class PublicMain extends Component {

    render = () => {
        return (
            <React.Fragment>
                <Header/>
                <div className="mdc-drawer-scrim"></div>
                <main className="mdc-layout__content">
                    <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem maxime corporis, aut ipsam sunt iste exercitationem, doloremque porro quibusdam aliquam, neque modi vitae eum placeat assumenda. Officiis libero fugiat quia?</span>
                </main>
            </React.Fragment>
        );
    }
}