import React, { Component } from 'react';
//import auth from '../../../../core/auth';
import {MDCTextField} from '@material/textfield/index';

export default class NovelView extends Component {

    componentDidMount = () => {
        for (const textfield of document.querySelectorAll('.mdc-text-field')) {
            new MDCTextField(textfield);
        }
    }

    render = () => {
        //const user = auth.GetUser();
        return (
            <React.Fragment>
                <div className="oricom_headerbar" style={{backgroundImage : "url(https://picsum.photos/1800/210/?random)"}}></div>
                <section className="oricom_novel-topbar">
                    <div className="cover-container">
                        <div className="cover">
                            <div className="fill fill-background" style={{backgroundImage : "url(https://picsum.photos/500/500/?random?1)"}}></div>
                        </div>
                    </div>
                    <div className="identity-container">
                        <span className="mdc-typography--headline5 text-ellipsis">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corrupti esse illo sunt suscipit hic reprehenderit.</span>
                        <div className="align-vertical-10" style={{width : "fit-content"}}>
                            <img className="avatar-small" alt="Marlon Cristian" src="https://picsum.photos/290/320/?random?2"></img>
                            <span className="mdc-typography--body2">Marlon Cristian</span>
                        </div>
                    </div>
                    <div className="align-horizontal">
                        <button className="mdc-fab mdc-ripple-upgraded default-action" aria-label="Favorite" style={{marginTop : -28}}>
                            <i className="mdc-fab__icon material-icons">edit</i>
                        </button>
                    </div>
                </section>

                <section className="section-divider-bottom" style={{backgroundColor: "#ececec"}}></section>
                
                <section className="section-container">
                    <header>Episódio</header>
                    <section className="text-field-container">
                        <span className="mdc-typography--body1">Lorem ipsum dolor sit, amet consectetur adipisicing elit. </span>
                    </section>
                    <section className="mdc-chip-set">
                        <div className="mdc-chip mdc-ripple-upgraded" id="mdc-chip-18">
                            <div className="mdc-chip__text">ficção cientifica</div>
                        </div>
                        <div className="mdc-chip mdc-ripple-upgraded" tabIndex="0" id="mdc-chip-18">
                            <div className="mdc-chip__text">terror</div>
                        </div>
                        <div className="mdc-chip mdc-ripple-upgraded" tabIndex="0" id="mdc-chip-18">
                            <div className="mdc-chip__text">anime</div>
                        </div>
                        <div className="mdc-chip mdc-ripple-upgraded" tabIndex="0" id="mdc-chip-18">
                            <div className="mdc-chip__text">scifi</div>
                        </div>
                    </section>
                </section>
                
                <section className="section-container">
                    <header>Episódios</header>
                    <section className="grid-small">
                        <div className="grid-thumb-small grid-thumb-dashed episode-new">
                            <span className="mdc-typography--subtitle2 align-horizontal">Crie, rascunhe e publique uma nova obra</span>
                            <i className="material-icons">add_circle_outline</i>
                        </div>
                        <div className="grid-thumb-small grid-thumb-active episode-element">
                            <div className="banner fill fill-background" style={{backgroundImage : "url('https://picsum.photos/290/320/?random?34343')"}}></div>
                            <div className="content">
                                <span className="mdc-typography--subtitle2 align-horizontal">Lorem, ipsum dolor sit amet consectetur adipisicing elit</span>
                            </div>
                            <div className="actions flex-container flex-right">
                                <button class="mdc-icon-button material-icons">edit</button>
                            </div>
                        </div>
                    </section>
                </section>
                
            </React.Fragment>
        )
    }
}