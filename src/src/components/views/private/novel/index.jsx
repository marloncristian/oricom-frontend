import NovelCreate from './create';
import NovelView from './view';
import EpisodeForm from './episode/form';
export {
    NovelCreate,
    NovelView,
    EpisodeForm
};