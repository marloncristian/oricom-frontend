import React, { Component } from 'react';
//import auth from '../../../../core/auth';
import {MDCTextField} from '@material/textfield/index';

export default class NovelCreate extends Component {

    componentDidMount = () => {
        for (const textfield of document.querySelectorAll('.mdc-text-field')) {
            new MDCTextField(textfield);
        }
    }

    render = () => {
        //const user = auth.GetUser();
        return (
            <React.Fragment>
                <div className="oricom_headerbar" style={{backgroundImage : "url(https://picsum.photos/1800/210/?random)"}}></div>
                <section className="oricom_novel-topbar">
                    <div className="cover-container">
                        <div className="cover">
                            <div className="fill fill-background" style={{backgroundImage : "url(https://picsum.photos/500/500/?random?1)"}}></div>
                            <div className="cover-action fill">
                                <div className="cover-action-overlay fill"></div>
                                <div className="cover-action-content fill">
                                    <i className="material-icons">cloud_upload</i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="identity-container">
                        <span className="mdc-typography--headline5 text-ellipsis">Dom casmurro e a ordem da fenix</span>
                        <div className="align-vertical-10" style={{width : "fit-content"}}>
                            <img className="avatar-small" alt="Marlon Cristian" src="https://picsum.photos/290/320/?random?2"></img>
                            <span className="mdc-typography--body2">Marlon Cristian</span>
                        </div>
                    </div>
                    <div className="actions-container align-horizontal">
                        <button className="mdc-top-app-bar__button mdc-button mdc-button--dense mdc-button--outlined">
                            <span className="mdc-button__label">cancelar</span>
                        </button>
                        <button className="mdc-top-app-bar__button mdc-button mdc-button--dense mdc-button--raised mdc-button--primary">
                            <span className="mdc-button__label">salvar</span>
                        </button>
                    </div>
                </section>

                <section className="section-divider-bottom" style={{backgroundColor: "#ececec"}}></section>

                <section className="section-container">
                    <header>Apresentação</header>
                    <section className="text-field-container">
                        <div className="mdc-text-field mdc-text-field--outlined mdc-text-field--with-trailing-icon mdc-text-field__autosize">
                            <input type="text" id="text-field-outlined" className="mdc-text-field__input" />
                            <i class="material-icons mdc-text-field__icon" tabindex="0" role="button">delete</i>
                            <div className="mdc-notched-outline">
                                <div className="mdc-notched-outline__leading">
                                </div>
                                <div className="mdc-notched-outline__notch">
                                    <label className="mdc-floating-label" htmlFor="text-field-outlined">Título</label>
                                </div>
                                <div className="mdc-notched-outline__trailing">
                                </div>
                            </div>
                        </div>
                        <div className="mdc-text-field-helper-line">
                            <p className="mdc-text-field-helper-text mdc-text-field-helper-text--persistent mdc-text-field-helper-text--validation-msg" id="pw-validation-msg">
                                O título é a identidade da sua obra. Ela será conhecida por este curto e suscinto texto, por isso faça valer a pena. Caracteres especiais não são aceitos aqui.
                            </p>
                        </div>
                    </section>
                    
                    <section className="text-field-container">
                        <div className="mdc-text-field mdc-text-field--textarea mdc-text-field--with-trailing-icon" style={{width : '100%'}}>
                            <textarea id="textarea" className="mdc-text-field__input" rows="8" style={{resize : "none"}}></textarea>
                            <i class="material-icons mdc-text-field__icon" tabindex="0" role="button">delete</i>
                            <div className="mdc-notched-outline">
                                <div className="mdc-notched-outline__leading"></div>
                                <div className="mdc-notched-outline__notch">
                                    <label for="textarea" className="mdc-floating-label">Descrição/Sinópse</label>
                                </div>
                                <div className="mdc-notched-outline__trailing"></div>
                            </div>
                        </div>
                        <div className="mdc-text-field-helper-line">
                            <p className="mdc-text-field-helper-text mdc-text-field-helper-text--persistent mdc-text-field-helper-text--validation-msg" id="pw-validation-msg">
                                A Sinopse é um breve relato, síntese ou sumário de uma obra. É ela que irá despertar o interesse e curiosidade dos leitores por isso tente ser o mais objetivo, evite os excessos e seja cativante.
                            </p>
                        </div>
                    </section>
                </section>
                
                <section className="section-container">
                    <header>Preferências</header>
                    <section className="text-field-container">
                        <div className="mdc-text-field mdc-text-field--outlined mdc-text-field--with-trailing-icon mdc-text-field__autosize">
                            <input type="text" id="text-field-outlined" className="mdc-text-field__input" />
                            <i class="material-icons mdc-text-field__icon" tabindex="0" role="button">delete</i>
                            <div className="mdc-notched-outline">
                                <div className="mdc-notched-outline__leading">
                                </div>
                                <div className="mdc-notched-outline__notch">
                                    <label className="mdc-floating-label" htmlFor="text-field-outlined">TAG</label>
                                </div>
                                <div className="mdc-notched-outline__trailing">
                                </div>
                            </div>
                        </div>
                        <div className="mdc-text-field-helper-line">
                            <p className="mdc-text-field-helper-text mdc-text-field-helper-text--persistent mdc-text-field-helper-text--validation-msg" id="pw-validation-msg">
                                Este campo é mais prático, ele irá facilitar o acesso a sua obra a partir das categorias que você irá identificar em suas tags. Por Exemplo: Terror, Ficção Científica, Aventura.
                            </p>
                        </div>
                        <div className="mdc-chip-set">
                            <div className="mdc-chip mdc-ripple-upgraded" tabindex="0" id="mdc-chip-18">
                                <i className="material-icons mdc-chip__icon mdc-chip__icon--leading" onClick={(e) => { console.log(e); } }>close</i>
                                <div className="mdc-chip__text">Ficção Cientifica</div>
                            </div>
                        </div>
                    </section>
                </section>
                
                <section className="section-divider-top" style={{backgroundColor: "#ececec"}}></section>

                <div className="section-action align-horizontal">
                    <button className="mdc-top-app-bar__button mdc-button mdc-button--dense mdc-button--outlined">
                        <span className="mdc-button__label">cancelar</span>
                    </button>
                    <button className="mdc-top-app-bar__button mdc-button mdc-button--dense mdc-button--raised mdc-button--primary">
                        <span className="mdc-button__label">salvar</span>
                    </button>
                </div>
            </React.Fragment>
        )
    }
}