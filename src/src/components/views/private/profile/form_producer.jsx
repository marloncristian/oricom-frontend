import React, { Component } from 'react';
import apiProducers from '../../../api/api_producers';
import auth from '../../../../core/auth';

export default class ProducerForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            producer : null,
            working : false
        }
    }

    componentDidMount = () => {
        this.getProducer();
    }

    getProducer = () => new Promise((resolve, reject) => {
        var userData = auth.GetUser();
        apiProducers.GetById(userData.id)
            .then((response) => {
                this.setState({producer : response.data});
                resolve(response.data);
            })
            .catch((err) => {
                if (err.response && err.response.status !== 404) {
                    window.M.toast({html: 'Não foi possível carregar o usuário'}, 10000);
                    reject(err.response.status);
                }
                resolve({});
            });
    })

    activateProducer = () => {
        this.setState({working : true});
        var userData = auth.GetUser();
        apiProducers.CreateAndRequest(userData.id)
            .then((response) => {
                this.setState({working : false});
                this.getProducer();
                window.M.toast({html: 'Requisição de ativação enviada'}, 10000);
            })
            .catch((err) => {
                this.setState({working : false});
                if (err.response.status !== 404) {
                    window.M.toast({html: 'Não foi possível carregar o usuário'}, 10000);
                }
            });
    }

    onActivateClick = (event) => {
        event.preventDefault();
        this.activateProducer();
    }

    render = () => {
        return (
            <div className="section-container oricom_user-container">
                <header>Producer</header>
                <section className="text-field-container">
                    <div className="mdc-text-field mdc-text-field--outlined" style={{width : '50%'}}>
                        <input type="text" id="text-field-outlined" className="mdc-text-field__input" />
                        <div className="mdc-notched-outline">
                            <div className="mdc-notched-outline__leading">
                            </div>
                            <div className="mdc-notched-outline__notch">
                                <label className="mdc-floating-label" htmlFor="text-field-outlined">Nome de producer</label>
                            </div>
                            <div className="mdc-notched-outline__trailing">
                            </div>
                        </div>
                    </div>
                    <div className="mdc-text-field-helper-line">
                        <p className="mdc-text-field-helper-text mdc-text-field-helper-text--persistent mdc-text-field-helper-text--validation-msg" id="pw-validation-msg">
                            Você tem a opção de se identificar com um nome diferente do seu nome de usuário. Assim você pode manter sua privacidade em suas publicações.
                        </p>
                    </div>
                </section>
                <section className="oricom_user-actions">
                    <button className="mdc-top-app-bar__button mdc-button mdc-button--dense mdc-button--raised mdc-button--primary">
                        <span className="mdc-button__label">atualizar</span>
                    </button>
                </section>
            </div>
        )
    }
}