import React, { Component } from 'react';
import auth from '../../../../core/auth';
import {MDCTextField} from '@material/textfield/index';
import FormProducer from './form_producer';
import FormProfile from './form_profile';
import FormNovels from './form_novels';

export default class Profile extends Component {


    constructor (props) {
        super(props);
        this.state = {
            loading : false,
        };
        this.profileRef = React.createRef();
        this.producerRef = React.createRef();
    }

    componentDidMount = () => {
        for (const textfield of document.querySelectorAll('.mdc-text-field')) {
            new MDCTextField(textfield);
        }

        /*
        this.setState({loading : true});
        Promise.all([
                this.profileRef.current.getUser(), 
                this.producerRef.current.getProducer()
            ]).then(response => {
                this.setState({loading : false});
            }).catch(err => {
                this.setState({loading : false});
            });
        */
    }

    render = () => {
        const user = auth.GetUser();
        return (
            <React.Fragment>
                <div className="oricom_user-headerbar oricom_headerbar" style={{backgroundImage : "url(https://picsum.photos/1800/210/?random)"}}>
                    <img className="avatar-large" src={user.picture_url} alt={user.name}></img>

                    <div className="oricom_user-details">
                        <span className="mdc-typography--headline5">{user.name}</span>
                        <span className="mdc-typography--headline6">{user.email || "Sem email"}</span>
                    </div>
                </div>

                <FormProfile/>
                
                {user.role.includes("producer") && <FormProducer/>}
                {user.role.includes("producer") && <FormNovels/>}

            </React.Fragment>
        )
    }
}