import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'

class FormNovels extends Component {

    handleNew = (e) => {
        this.props.history.push('/private/novel/create')
    }

    render = () => {
        return (
            <div className="section-container oricom_user-container">
                <header>Obras</header>
                <section className="grid-small">
                    <div className="grid-thumb-small oricom_user-novel_thumb_new" onClick={(e) => this.handleNew(e)}>
                        <h1 className="mdc-typography--subtitle2">Crie, rascunhe e publique uma nova obra</h1>
                        <i className="material-icons">add_circle_outline</i>
                    </div>

                    <div className="grid-thumb-small oricom_user-novel_thumb_element">
                        <div className="cover" style={{backgroundImage : "url(https://picsum.photos/200/300?random&1)"}}></div>
                        <h1 className="mdc-typography--subtitle2">Sample Novel</h1>
                    </div>
                    
                    <div className="grid-thumb-small oricom_user-novel_thumb_element">
                        <div className="cover" style={{backgroundImage : "url(https://picsum.photos/200/300?random&2)"}}></div>
                        <h1 className="mdc-typography--subtitle2">Sample Novel</h1>
                    </div>
                    
                    <div className="grid-thumb-small oricom_user-novel_thumb_element">
                        <div className="cover" style={{backgroundImage : "url(https://picsum.photos/200/300?random&3)"}}></div>
                        <h1 className="mdc-typography--subtitle2">Sample Novel</h1>
                    </div>
                    
                    <div className="grid-thumb-small oricom_user-novel_thumb_element">
                        <div className="cover" style={{backgroundImage : "url(https://picsum.photos/200/300?random&4)"}}></div>
                        <h1 className="mdc-typography--subtitle2">Sample Novel</h1>
                    </div>
                    
                    <div className="grid-thumb-small oricom_user-novel_thumb_element">
                        <div className="cover" style={{backgroundImage : "url(https://picsum.photos/200/300?random&5)"}}></div>
                        <h1 className="mdc-typography--subtitle2">Sample Novel</h1>
                    </div>
                    
                    <div className="grid-thumb-small oricom_user-novel_thumb_element">
                        <div className="cover" style={{backgroundImage : "url(https://picsum.photos/200/300?random&6)"}}></div>
                        <h1 className="mdc-typography--subtitle2">Sample Novel</h1>
                    </div>
                    
                    <div className="grid-thumb-small oricom_user-novel_thumb_element">
                        <div className="cover" style={{backgroundImage : "url(https://picsum.photos/200/300?random&7)"}}></div>
                        <h1 className="mdc-typography--subtitle2">Sample Novel</h1>
                    </div>

                </section>
            </div>
        )
    }
}

export default withRouter(FormNovels)