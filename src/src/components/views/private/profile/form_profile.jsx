import React, { Component } from 'react';
import apiUsers from '../../../api/api_users';
import auth from '../../../../core/auth';
import LinearProgressBar from '../_shared_/linear_progressbar'

export default class ProfileForm extends Component {

    constructor (props) {
        super(props);
        this.state = {
            profile : {
                Name : ''
            },
            working : false
        };
        this.ProgressRef = React.createRef();
    }

    componentDidMount = () => {
        this.getUser();
    }

    getUser = () => new Promise((resolve, reject) => {
        apiUsers.GetMe()
            .then((response) => {
                this.setState({ profile : response.data });
                resolve(response.data);
            })
            .catch((err) => {
                console.log(err);
                //window.M.toast({html: 'Não foi possível carregar o usuário'}, 10000);
                reject(err);
            });
    })

    updateUser = (data) => {
        this.setState({ working : true });
        var userData = auth.GetUser();
        this.ProgressRef.current.show();
        apiUsers.Update(userData.id, data)
            .then((response) => {
                this.setState({ profile : response.data, working : false });
                //window.M.toast({html: 'Usuário atualizado com sucesso'}, 10000);
                this.setState({ working : false });
                this.ProgressRef.current.hide();
            })
            .catch((error) => {
                this.setState({ working : false });
                //window.M.toast({html: 'Não foi possível atualizar o usuário'}, 10000);
                this.ProgressRef.current.hide();
            });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.updateUser({Name : this.state.profile.Name});
    }

    onChangeName = (event) => {
        event.preventDefault();
        var profile = this.state.profile;
        profile.Name = event.target.value;
        this.setState(profile);
    }

    render = () => {
        return (
            <div className="section-container oricom_user-container">
                <header>Perfil</header>
                <section className="text-field-container">
                    <div className="mdc-text-field mdc-text-field--outlined" style={{width : '50%'}}>
                        <input type="text" id="text-field-outlined-name" className="mdc-text-field__input" value={this.state.profile.Name} onChange={e => this.onChangeName(e) } />
                        <div className="mdc-notched-outline mdc-notched-outline--upgraded mdc-notched-outline--notched">
                            <div className="mdc-notched-outline__leading"></div>
                            <div className="mdc-notched-outline__notch">
                                <label className="mdc-floating-label mdc-floating-label--float-above" htmlFor="text-field-outlined-name">Nome</label>
                                <div class="mdc-line-ripple"></div>
                            </div>
                            <div className="mdc-notched-outline__trailing">
                            </div>
                        </div>
                    </div>
                    <div className="mdc-text-field-helper-line">
                        <p className="mdc-text-field-helper-text mdc-text-field-helper-text--persistent mdc-text-field-helper-text--validation-msg" id="pw-validation-msg">
                            É assim que você será indificado dentro do site e em nossas comunicações. Sugerimos usar o padrão nome seguido de 1 ou 2 sobrenomes. Ex.: Barack Obama ou Barack Hussein Obama.
                        </p>
                    </div>
                </section>

                <section className="oricom_user-actions">
                    <button className="mdc-top-app-bar__button mdc-button mdc-button--dense mdc-button--raised mdc-button--primary" onClick={e => this.handleSubmit(e)} disabled={this.state.working}>
                        <span className="mdc-button__label">atualizar</span>
                    </button>
                </section>
                
                <section>
                    <LinearProgressBar ref={this.ProgressRef}/>
                </section>

            </div>       
        )
    }
}