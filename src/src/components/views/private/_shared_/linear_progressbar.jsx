import React, { Component } from 'react';

export default class LinearProgressBar extends Component {

    constructor (props) {
        super(props);
        this.ProgressRef = React.createRef();
    }

    show = () => {
        this.ProgressRef.current.style.height = null;
    }

    hide = () => {
        this.ProgressRef.current.style.height = "0px";
    }

    render = () => {
        return (
            <div ref={this.ProgressRef} role="progressbar" className="transitionate loader mdc-linear-progress mdc-linear-progress--indeterminate" style={{ position: 'relative', top: 0, left: 0, zIndex: '101', height : 0 }}>
                <div className="mdc-linear-progress__buffering-dots"></div>
                <div className="mdc-linear-progress__buffer"></div>
                <div className="mdc-linear-progress__bar mdc-linear-progress__primary-bar">
                    <span className="mdc-linear-progress__bar-inner"></span>
                </div>
                <div className="mdc-linear-progress__bar mdc-linear-progress__secondary-bar">
                    <span className="mdc-linear-progress__bar-inner"></span>
                </div>
            </div>
        )
    }
}