import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from './header'
import Profile from '../profile/'
import * as Novel from '../novel/'

export default class PrivateMain extends Component {
    render = () => {
        return (
            <React.Fragment>
                <Header></Header>
                <Router>
                    <Switch>
                        <Route exact path="/private/" component={Profile}></Route>
                        <Route path="/private/novel/create" component={Novel.NovelCreate}></Route>
                        <Route path="/private/novel/view" component={Novel.NovelView}></Route>
                        <Route path="/private/novel/x/episode" component={Novel.EpisodeForm}></Route>
                    </Switch>
                </Router>
            </React.Fragment>
        )
    }
}