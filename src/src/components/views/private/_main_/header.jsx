import React, { Component } from 'react';

export default class Header extends Component {

    render () {
        return (
            <React.Fragment>
                <header id="app-bar" className="mdc-top-app-bar mdc-top-app-bar--fixed" ref={(element) => { this.appBarEle = element; }}>
                    <div className="mdc-top-app-bar__row">
                        <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
                            <a href='#!' className="material-icons mdc-top-app-bar__navigation-icon">home</a>
                            <span className="mdc-top-app-bar__title">Oricom</span>
                        </section>
                    </div>
                </header>
                <div className="oricom_header-adjust"></div>
            </React.Fragment>
        );
    }
}