import axios from 'axios';
import auth from '../../core/auth';
import constants from '../../constants';

var authorizationHeader = { headers: { "authorization" : `bearer ${auth.GetToken()}` } };

export default {

    GetById : (id) => axios.get(`${constants.Apis.Foundation}v1/producers/${id}`, authorizationHeader),

    GetMe : () => axios.get(`${constants.Apis.Foundation}v1/me/producers`, authorizationHeader),

    CreateAndRequest : (displayName) => axios.post(`${constants.Apis.Foundation}v1/me/producers`, { DisplayName : displayName }, authorizationHeader)
}