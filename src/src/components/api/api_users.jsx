import axios from 'axios';
import auth from '../../core/auth';
import constants from '../../constants';

export default {
    GetAll : () => axios.get(
        `${constants.Apis.Security}v1/users?page=0`, 
        { headers: { "authorization" : `bearer ${auth.GetToken()}` } 
    }),

    GetByName : (name) => axios.get(
        `${constants.Apis.Security}v1/users?page=0&name=${name}`, 
        { headers: { "authorization" : `bearer ${auth.GetToken()}` } }
    ),

    GetById : (id) => axios.get(
        `${constants.Apis.Security}v1/users/${id}`, 
        { headers: { "authorization" : `bearer ${auth.GetToken()}` } 
    }),

    GetMe : () => axios.get(
        `${constants.Apis.Security}v1/me/users`, 
        { headers: { "authorization" : `bearer ${auth.GetToken()}` } 
    }),

    Update : (id, data) => axios.patch(
        `${constants.Apis.Security}v1/users/${id}`, data, 
        { headers: { "authorization" : `bearer ${auth.GetToken()}` } }
    ),
}