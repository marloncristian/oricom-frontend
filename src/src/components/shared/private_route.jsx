import React from 'react'
import auth from '../../core/auth'
import { Route, Redirect } from 'react-router'

const PrivateRoute = ({ component: Component, ...rest }) => {

  const isAuthenticated = auth.IsAuthenticated()

  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/', state: { from: props.location } }} />
        )
      }
    />
  )
}

export default PrivateRoute