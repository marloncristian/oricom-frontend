export default {
    LoginUrl : "https://oricom-security.herokuapp.com/auth",
    ClientID : "320860820340-kn1eqblbdfunves3n8dl3hvbcco3ng41.apps.googleusercontent.com",

    Apis : {
        Security : "https://oricom-security.herokuapp.com/",
        Foundation : "https://oricom-foundation.herokuapp.com/"
    }
}