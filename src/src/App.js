import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PrivateRoute from './components/shared/private_route';

import PrivateMain from './components/views/private/_main_/'
import PublicMain from './components/views/public/_main_/'

import './styles/main.scss';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={PublicMain}></Route>
          <PrivateRoute path="/private/" component={PrivateMain}></PrivateRoute>
        </Switch>
      </Router>
    );
  }
}

export default App;
