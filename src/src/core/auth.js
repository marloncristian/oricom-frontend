import Cookies from 'js-cookie';
import Decode from 'jwt-decode';

export default {

    IsTokenValid() {
        var token = Cookies.get('access_token');
        if (token === undefined) {
            return false;
        }

        var {exp} = Decode(token);
        if (exp < new Date().getTime() / 1000){
            return false;
        }
        return true;
    },

    IsAuthenticated() {
        return this.IsTokenValid()
    },

    SignIn(token, user) {
        Cookies.set('access_token', token, { expires: 2 });
        Cookies.set('oricom_user', JSON.stringify(user), { expires: 2 });
    },

    SignOut() {
        Cookies.remove('access_token');
        Cookies.remove('oricom_user');
    },

    GetUser() {
        return Cookies.getJSON('oricom_user');
    },

    GetToken() {
        return Cookies.get('access_token');
    }
}