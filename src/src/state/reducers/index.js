import { combineReducers } from 'redux';
import { AuthStatusChangeReducer } from './reducer_authentication';

export const reducers = combineReducers({
    authenticationState : AuthStatusChangeReducer
})