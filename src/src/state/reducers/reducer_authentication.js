import auth from '../../core/auth'
import { ACT_AUTHENTICATION_STATUS_CHANGE } from '../actions/action_types';

const initialState = {
    authenticationStatus : auth.IsAuthenticated()
}

export const AuthStatusChangeReducer = (state = initialState, action) => 
{
    switch (action.type) {
        case ACT_AUTHENTICATION_STATUS_CHANGE : 
            console.log(ACT_AUTHENTICATION_STATUS_CHANGE, action.authenticationStatus);
            return {...state, authenticationStatus : action.authenticationStatus };
        default:
            return state;
    }
}