import {ACT_AUTHENTICATION_STATUS_CHANGE} from './action_types';

export const actionAuthStatusChange = (value) => ({
    type : ACT_AUTHENTICATION_STATUS_CHANGE,
    authenticationStatus : value
});