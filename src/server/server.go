package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
)

// Server has router and db instances
type Server struct {
}

// Initialize initializes the app with predefined configuration
func (a *Server) Initialize() {
	http.HandleFunc("/", a.handleRequests)
}

func (a *Server) getBasePath() string {
	basePath := "build"
	if os.Getenv("ORI_ENV") == "development" {
		basePath = path.Join("..", "build")
	}
	return basePath
}

// handleRequests handles static files request
func (a *Server) handleRequests(w http.ResponseWriter, r *http.Request) {
	if filepath.Ext(r.URL.Path) == "" {
		http.ServeFile(w, r, path.Clean(path.Join(a.getBasePath(), "index.html")))
	} else {
		http.ServeFile(w, r, path.Clean(path.Join(a.getBasePath(), r.URL.Path)))
	}
}

// Run the app on it's router
func (a *Server) Run() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}
	log.Printf("listening do port: %s", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}

// NewServer creates a new instance of server
func NewServer() *Server {
	return &Server{}
}
