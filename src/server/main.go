package main

import (
	"log"
	"os"
	//"oricom.io/ui/web"
)

// main rest api endpoint
func main() {
	log.SetOutput(os.Stdout)
	log.Print("initializing...")

	//initializes the api server
	server := NewServer()
	server.Initialize()
	server.Run()

	log.Print("server running...")
}
