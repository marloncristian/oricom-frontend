
## Development

Problemas com o sass-loader

https://github.com/material-components/material-components-web-react/blob/v0.6.2/docs/adding-env-variables.md

`SETX SASS_PATH .\node_modules`

## Deployment

Deploying the application to Heroku in production

### Build app static files

`npm run build`

### Build docker image on heroku registry

`docker build -t registry.heroku.com/oricom/web .`

### Publish docker image to heroku registry

`docker push registry.heroku.com/oricom/web`

### Deploy to Heroku

`heroku container:release web --app oricom`